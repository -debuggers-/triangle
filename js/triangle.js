(function(game, $) 
{
    // global variables and settings of game
    game.prototype = 
    {
            startlvl : 1,
        blockStartId : 1,
               $path : $('#path'),
               $main : $('#main'),
                mode : 'letters',
          pathValues : []
    };

    // rounded block identifier (unique)
    game.id = game.prototype.blockStartId;

    // array containing current path elements
    game.way = [];

    // gets last element from array
    Array.prototype.last = function() 
    {
        return this[this.length-1];
    }

    // supports formatted string processing
    String.prototype.supplant = function supplant(dictionary) 
    {
        return this.replace(/{([^{}]*)}/g,
            function (match, key) 
            {
                var value = dictionary[key];

                return (typeof value === 'string' || typeof value === 'number') ? 
                    value : (typeof value === 'function') ? 
                    value() : match;
            }
        );
    };

    // get element's both children by target id and make them active
    game.activateChildren = function($element) 
    {
        var left = $element.attr('data-left-child'),
           right = $element.attr('data-right-child');

        $('span#' + left + ', span#' + right).addClass('active');
    };

    // create and insert blocks into rows
    game.appendBlocks = function(rowNumber, depth) 
    {
        for (var position = 0; position < rowNumber; position++) 
        {
            var params = 
            {
                          row : rowNumber,
                           id : 'block-' + (this.id++),
                 leftTargetId : 'block-' + (this.id + rowNumber - 1),
                rightTargetId : 'block-' + (this.id + rowNumber)
            },

            element = '<span id="{id}" class="block" onclick="triangle.unblockNextRow(this)" ';

            element = (rowNumber < depth) ? 
                element += 'data-left-child="{leftTargetId}" data-right-child="{rightTargetId}"></span>' : element + '></span>';

            $('div.row:nth-child({row})'.supplant(params)).append(element.supplant(params)); 
        }
    };

    // establish containers for numeric blocks based on level
    game.appendRows = function(rows) 
    {
        for (var height = 0; height < rows; height++) 
        {
            this.prototype.$main.append('<div class="row">');
            this.appendBlocks(height + 1, rows);
        }

        $('div.row').css('min-width', 50 * rows);
    };

    // determine block's position in row
    game.checkForRenewal = function(level) 
    {
        var content = level.row.block.content;

        if (!(content.is(':first-child' || content.is(':last-child')))) 
        {
            this.recoverValue(level.row, level.path);
        }
    };

    // determine whether path values are unique
    game.checkPath = function(associative, $item) 
    {
        var dividedObject = this.getObjectKeysAndValues(associative),
            createdWayIds = dividedObject.keys,
               createdWay = dividedObject.values,
                    depth = this.prototype.$main.children().length;

        var hasUniqueValues = (createdWay.filter(function(value, index, self) 
        { 
            return (self.indexOf(value) !== index);
        }).length === 0) ? true : false;

        if (!hasUniqueValues) 
        {
            this.markErrorInitiators(associative, $item);
        }

        if (!hasUniqueValues || createdWay.length === depth) 
        {
            var reducedHeight = (!hasUniqueValues && depth > 2) ? 
                Math.floor(depth * 0.9) : depth,

                   params = 
                   {
                        textColor : (hasUniqueValues) ? 
                            'text-success' : 'text-danger',

                           result : (hasUniqueValues) ? 
                                'Apsveicu! Virkne ir pareiza' : (createdWay.length < depth) ?
                                    'Galapunkts šoreiz netiks sasniegts' : 'Diemžēl virkne ir nepareiza',

                      buttonClass : (hasUniqueValues) ? 
                        'success' : 'danger',

                       buttonText : (hasUniqueValues) ? 
                            'Turpināt' : (depth > 2) ? 'Mēgināt vēlreiz ar zemāku līmeni' : 'Sākt no sākuma',
                       level : (hasUniqueValues) ?
                            ++depth : reducedHeight
                   },

                   button = '<button id="next-lvl" type="button" class="btn btn-primary btn-lg btn-block btn-{buttonClass}" onclick="triangle.generateLevel({level})">{buttonText}</button>'.supplant(params);

            $('#result').append('<h3 id="way" class="{textColor}">{result}</h3>'.supplant(params));
            $('#navigator').append(button);

            this.deactivateChildren($item);
        }
    };

    // remove all rounded blocks within its container
    game.clearGameField = function() 
    {
        var self = this;

        self.prototype.$main.children().each(function() 
        {
            self.prototype.$main[0].removeChild(this);
        });

        return this;
    }
    
    // erases all path content
    game.clearPath = function() 
    {
        this.prototype.$path.empty();

        return this;
    }

    // get element's both children by target id and make them disabled
    game.deactivateChildren = function($element) 
    {
        var left = $element.attr('data-left-child'),
           right = $element.attr('data-right-child');

        $('span#' + left + ', span#' + right).removeClass('active');
    }

    // assign values to non-path blocks
    game.fillOtherBlocks = function() 
    {
        var level = 
        {
            depth : this.prototype.$main.children().length,
             path : this.prototype.pathValues,
        };

        this.passThroughRows(level);

        level = null;
    };

    // assign values to randomly left or right bottom direction choosen path blocks
    game.fillPathBlocks = function() 
    {
        var depth = this.prototype.$main.children().length,
           $block = $('div.row:first-child').children().first().addClass('path'),
               id = parseInt($block.attr('id').substr('block-'.length));

         var path = (this.prototype.mode === 'letters') ?
            this.shuffle(this.getLetters(depth)) : this.shuffle(this.getNumbers(depth));
        
        this.way[id] = path[0];
        this.prototype.$path.append(path[0] + ' ');
        this.activateChildren($block);

        for (var lvl = 0; lvl < depth; lvl++) 
        {
            var nextId = (Math.random() < 0.5) ? $block.attr('data-left-child') : $block.attr('data-right-child');

            $block.text(path[lvl]);
            $block = $('span#' + nextId);
        }

        this.prototype.pathValues = path;
    };

    // create game field layout
    game.generateLevel = function(height) 
    {
        var depth = ($.isNumeric(height) && height > 1) ? 
            height : 2;

        if (this.prototype.$main.children().length) 
        {
            this.prepareField();
        }

        this.appendRows(depth);
        this.fillPathBlocks();
        this.fillOtherBlocks();
    };

    // return sequence of letters
    game.getLetters = function(count, lang = 'lat') 
    {
        var src = this.getAlphabet(lang),
         length = src.length,
            arr = [];

        for (var i = 0; i < count; i++) 
        {
            var index = Math.floor(Math.random() * length);
            if (index < length--) 
            {
                var letter = src.splice(src.indexOf(src[index]), 1).toString();
                arr.push(letter);
            }
        }

        return arr;
    };

    game.getAlphabet = function(lang) 
    {
        switch(lang) {
            case 'lat':
                return ['a', 'ā', 'b', 'c', 'č', 'd', 'e', 'ē', 'f', 'g', 'ģ', 'h', 'i', 'ī', 'j', 'k', 'ķ', 'l', 'ļ', 'm', 'n', 'ņ', 'o', 'p', 'r', 's', 'š', 't', 'u', 'ū', 'v', 'z', 'ž'];
            case 'rus':
                return ['а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я'];
            case 'eng':
                return ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'];
            default:
                console.log('Alphabet doesn\'t exists');
                break;
        }
    };

    // return sequence of converted to string elements specified by interval
    game.getNumbers = function(limit) 
    {
        var arr = [],
          start = 1;

        while (arr.length !== limit) 
        {
            arr.push((start++).toString());
        }

        return arr;
    };

    // split associative array into key and value arrays
    game.getObjectKeysAndValues = function(object) 
    {
        var keys = [],
          values = [];

        Object.keys(object).forEach(function(key) 
        {
            if (object.hasOwnProperty(key)) 
            {
                keys.push(key);
                values.push(object[key]);
            }
        });

        return { keys : keys, values : values };
    };

    // prepare raw row elements converted to array
    game.getRowElements = function($blocks) 
    {
        var elements = [];

        $blocks.each(function() 
        {
            elements.push($(this).text());
        });

        return elements;
    };

    // fill non-path block with value
    game.insertValue = function(row, levelDepth) 
    {
        var   block = row.block,
               pool = row.pool,
         currentRow = row.arrays.currRow,
        previousRow = row.arrays.prevRow,
              value = 0;

        if (!block.content.text().length) 
        {
            if (pool.length) 
            {
                value = (levelDepth > 2) ? pool.splice(0, 1).toString() : pool[0];
            }

            else 
            {
                var backup = this.prototype.pathValues.slice(0),
                 strongExc = [previousRow[block.id - 1], previousRow[block.id], currentRow[block.id - 1], currentRow[block.id + 1]],
                   weakExc = [currentRow[block.id - 2], currentRow[block.id - 1], currentRow[block.id + 1]],
                  removals = (levelDepth > 4) ? strongExc : weakExc;

                this.removeMultipleValues(backup, removals);
                
                value = backup.splice(0, 1).toString();
            }

            block.content.text(value);
            currentRow[block.id] = value;
        }
    };

    // highlight two blocks with equal values
    game.markErrorInitiators = function(indexedArray, $element) 
    {
        var params = 
        {
             first : $.inArray($element.text(), indexedArray).toString(),
            second : $element.attr('id').substr('block-'.length)
        };

        $('span#block-{first}, span#block-{second}'.supplant(params)).addClass('error');
    };

    // navigate through all blocks in row
    game.passThroughBlocks = function(level) 
    {
        var $children = level.row.content.children();

        for (var position = 0, rowLength = $children.length; position < rowLength; position++) 
        {
            level.row.block = 
            {
                     id : position,
                content : $children.eq(position)
            };

            this.processBlock(level);
        }

        level.row.block = null;
    };

    // navigate through all rows
    game.passThroughRows = function(level) 
    {
        for (var index = 1; index < level.depth; index++) 
        {
            var $line = $('div.row');

            level.row = 
            {
                     id : index,
                content : $line.eq(index),
                   pool : level.path.slice(0),
                 arrays : {
                    prevRow : this.getRowElements($line.eq(index - 1).children()),
                    currRow : this.getRowElements($line.eq(index).children()),
                    nextRow : this.getRowElements($line.eq(index + 1).children())
                }
            };

            this.removeExistingValue(level.row.pool, level.path[index]);
            this.passThroughBlocks(level);

            level.row = null;
        }
    };

    // clear current level field and prepare it for next level
    game.prepareField = function() 
    {     
        this.resetState();

        var     $level = $('#level'),
            $nextLevel = $('#next-lvl'),
                   lvl = parseInt($('#level').text());

        lvl = ($nextLevel.hasClass('btn-success')) ? ++lvl : (lvl > 1) ? Math.floor(lvl * 0.9) : lvl;

        var params = 
        {
            next: lvl
        }; 

        $level.text('{next}. līmenis'.supplant(params));
        $nextLevel.remove();

        $('#numbers, #way').remove();
    };

    // determine block's position in row
    game.processBlock = function(level) 
    {
        var  pool = level.row.pool,
           arrays = level.row.arrays,
            block = level.row.block,
        nextRowId = level.row.id + 1,
        condition = arrays.nextRow[block.id] === level.path[nextRowId] || arrays.nextRow[block.id + 1] === level.path[nextRowId];

        if (level.depth > 2) 
        {
            this.removeExistingValue(pool, arrays.prevRow[block.id]);
        }

        else if (condition && level.depth > 3) 
        {
            this.removeExistingValue(pool, level.path[nextRowId]);
        }

        this.insertValue(level.row, level.depth);
        this.checkForRenewal(level);
    };

    // if possible, restore upper row left block's value to pool 
    game.recoverValue = function(row, path) 
    {
        var recover = row.arrays.prevRow[row.block.id - 1],
               pool = row.pool,
         currentRow = row.arrays.currRow,
        pathElement = path[row.id],
          condition = pool.indexOf(recover) === -1 && currentRow.indexOf(recover) === -1 && recover !== pathElement;

        if (condition) 
        {
            pool.push(recover);
            this.shuffle(pool);
        } 
    };

    // take out already existing element in pool or backup pool
    game.removeExistingValue = function(arr, value) 
    {
        var removeIndex = arr.indexOf(value);

        if (removeIndex !== -1) 
        {
            var removed = arr.splice(removeIndex, 1);
        }
    };

    // take out multiple already existing elements in backup pool
    game.removeMultipleValues = function(arr, discards) 
    {
        for (var i = 0, length = discards.length; i < length; i++) 
        {
            this.removeExistingValue(arr, discards[i]);
        }
    };

    // restore initial game settings
    game.resetState = function() 
    {
        this.clearGameField();
        this.clearPath();
        this.id = this.prototype.blockStartId;
        this.prototype.pathValues.length = 0;
        this.way.length = 0;
    };

    // randomly swap order of elements in array
    game.shuffle = function(arr) 
    {
        for (var curr = 0, arrLen = arr.length; curr < arrLen; curr++) 
        {
            var next = Math.floor(Math.random() * (curr + 1)),
                 tmp = arr[curr];

            arr[curr] = arr[next];
            arr[next] = tmp;
        }

        return arr;
    };

    game.switchMode = function() 
    {
         var $caption = $('#title'),
       replaceCaption = $caption.text().substring(0, $caption.text().indexOf('Trijstūris')).trim(),

      exchangeCaption = (this.prototype.mode === 'letters') ? 
        'Skaitļu' : 'Burtu',

           $operation = $('#switch'),
     replaceOperation = $operation.text().substring('Pārslēgties uz '.length),

    exchangeOperation = (this.prototype.mode === 'letters') ?
        'burtiem' : 'skaitļiem',

             $message = $('#message'),
       replaceMessage = $message.text().substring('Jūsu '.length, $message.text().indexOf('virkne:')).trim(),

      exchangeMessage = (this.prototype.mode === 'letters') ?
      'skaitļu' : 'burtu',

               $level = $('#level'),
                 mode = (this.prototype.mode === 'letters') ? 
                    'numbers' : 'letters';

        $level.text($level.text().replace(parseInt($level.text()), this.prototype.startlvl));

        $caption.text($caption.text().replace(replaceCaption, exchangeCaption));
        $operation.text($operation.text().replace(replaceOperation, exchangeOperation));
        $message.text($message.text().replace(replaceMessage, exchangeMessage));
        this.prototype.mode = mode;

        this.generateLevel();
    };

    // change available elements to click
    game.unblockNextRow = function(element) 
    {
        var $block = $(element),
         leftChild = $block.attr('data-left-child'),
        rightChild = $block.attr('data-right-child');

        if ($block.hasClass('active')) 
        {
            var id = parseInt($block.attr('id').substr('block-'.length)),
              path = this.prototype.$path;

            params = 
            {
                left  : id - 1,
                right : id + 1
            };

            $('span#block-{left}, span#block-{right}'.supplant(params)).removeClass('active');
            $block.removeClass('active').addClass('path');
            path.append('{value} '.supplant({value: $block.text()}));

            if ((typeof leftChild !== undefined || !leftChild) && (typeof rightChild !== undefined || !rightChild)) 
            {
                this.activateChildren($block);
            }

            var lastPathElement = path.text().trim().split(' ').last();

            this.way[id] = lastPathElement;
            this.checkPath(this.way, $block);
        }
    };
})(window.triangle || (window.triangle = {}), jQuery);

$(document).ready(function() 
{
    triangle.generateLevel();
});